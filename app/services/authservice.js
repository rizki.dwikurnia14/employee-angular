app.factory('AuthService', ['$rootScope', function($rootScope) {
    var authService = {};
    
    function getCurrentUserFromStorage() {
        var user = localStorage.getItem('currentUser');
        return user ? JSON.parse(user) : null;
    }

    authService.currentUser = getCurrentUserFromStorage();

    authService.login = function(credentials) {
        //harusnya manggil backend buat validasi login
        if (credentials.username === 'user' && credentials.password === 'user') {
            authService.currentUser = {
                username: credentials.username,
            };
            localStorage.setItem('currentUser', JSON.stringify(authService.currentUser));
            $rootScope.isLoggedIn = true
            return true;
        }
        $rootScope.isLoggedIn = false
        return false;
    };

    authService.logout = function() {
        authService.currentUser = null;
        localStorage.removeItem('currentUser');
        $rootScope.isLoggedIn = false
    };

    authService.isAuthenticated = function() {
        return !!authService.currentUser;
    };

    authService.isAuthorized = function(authorizedRoles) {
        if (!Array.isArray(authorizedRoles)) {
            authorizedRoles = [authorizedRoles];
        }
        return authService.isAuthenticated() && authorizedRoles.includes(authService.currentUser.role);
    };

    return authService;
}]);
