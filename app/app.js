var app = angular.module("myApp", ["ngRoute"]);

app.config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "views/home.html",
            controller: "MainController",
            resolve: {
                auth: function (AuthService, $location) {
                    if (!AuthService.isAuthenticated()) {
                        $location.path("/login");
                        return false
                    }else{
                        return true
                    }
                },
            },
        })
        .when("/detail/", {
            redirectTo: "/",
        })
        .when("/detail/:username", {
            templateUrl: "views/detail.html",
            controller: "DetailController",
            resolve: {
                auth: function (AuthService, $location) {
                    if (!AuthService.isAuthenticated()) {
                        $location.path("/login");
                        return false
                    }else{
                        return true
                    }
                },
            },
        })
        .when("/new", {
            templateUrl: "views/new.html",
            controller: "NewEmployeeController",
            resolve: {
                auth: function (AuthService, $location) {
                    if (!AuthService.isAuthenticated()) {
                        $location.path("/login");
                        return false
                    }else{
                        return true
                    }
                },
            },
        })
        .when("/login", {
            templateUrl: "views/login.html",
            controller: "LoginController",
            resolve: {
                auth: function (AuthService, $location) {
                    if (AuthService.isAuthenticated()) { //klo ini cek udh login apa belom biar gabias login lagi
                        $location.path("/");
                        return false
                    }else{
                        return true
                    }
                },
            },
        })
        .when("/logout", {
            templateUrl: "views/login.html",
            controller: "LogoutController",
        })
        .otherwise({ redirectTo: "/" });

    $locationProvider.hashPrefix("");
});
