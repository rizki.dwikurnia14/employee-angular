app.controller('LoginController', ['$scope', '$location', 'AuthService', function($scope, $location, AuthService) {
    $scope.credentials = {
        username: '',
        password: ''
    };

    $scope.login = function() {
        if (AuthService.login($scope.credentials)) {
            $location.path('/');
        } else {
            alert('Invalid username or password');
        }
    };
}]);
