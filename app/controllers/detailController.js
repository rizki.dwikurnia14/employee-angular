app.controller("DetailController", function ($scope, $routeParams, $location, employeesService) {
    $scope.title = "Add New Employee";

    $scope.dataDetail = employeesService.findEmployeeByUsername($routeParams.username)
    console.log($scope.dataDetail)

    $scope.EmployeeInput = [{}];

    $scope.cancel = function () {
        $location.path('/')
        return
    };

    $scope.validateNumberInputSalary = function () {
        $scope.EmployeeInput.basicSalary = $scope.EmployeeInput.basicSalary.replace(/\D/g, '');
        $scope.EmployeeInput.basicSalary = $scope.EmployeeInput.basicSalary.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    };
});
