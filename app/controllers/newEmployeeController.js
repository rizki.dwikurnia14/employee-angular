app.controller("NewEmployeeController", function ($scope, $timeout, $location) {
    $scope.title = "Add New Employee";

    $scope.today = new Date().toISOString().split("T")[0];
    document.getElementById("employeeBirthDate").setAttribute("max", $scope.today);
    document.getElementById("employeeDescription").setAttribute("max", $scope.today);

    $scope.EmployeeInput = [{}];

    $scope.cancel = function () {
        $location.path('/')
        return
    };

    $scope.validateNumberInputSalary = function () {
        $scope.EmployeeInput.basicSalary = $scope.EmployeeInput.basicSalary.replace(/\D/g, '');
        $scope.EmployeeInput.basicSalary = $scope.EmployeeInput.basicSalary.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    };

    // Load Data Table setelah view di render
    $timeout(function () {}, 0);
});
