angular.module('myApp').controller("MainController", function ($scope, $rootScope, $timeout, $filter, $location, employeesService) {
    $scope.staticEmployees = employeesService.allEmployee();

    $scope.title = "Employee List";

    $scope.searchBar = false;
    $scope.searchParam1 = null;
    $scope.searchParam2 = null;
    $scope.searchOptions1 = null;
    $scope.searchOptions2 = null;

    if ($rootScope.historySearch == undefined || $rootScope.historySearch.searched === null) {
        $rootScope.historySearch = {};
    }

    $scope.toggleSearch = function () {
        $scope.searchBar = !$scope.searchBar;
    };

    $scope.checkIfDate = function (variable) {
        if (variable instanceof Date) {
            variable.setHours(0, 0, 0, 0);
            return variable.toISOString().toLowerCase();
        } else {
            return variable.toLowerCase();
        }
    };

    $scope.formatDate = function (date) {
        date = new Date(date);
        let day = String(date.getDate()).padStart(2, "0");
        let month = String(date.getMonth() + 1).padStart(2, "0");
        let year = date.getFullYear();
        return day + "/" + month + "/" + year;
    };

    $scope.refresh = function () {
        var dataTableData = $scope.staticEmployees.map((item) => [
            item.username,
            item.firstName,
            item.lastName,
            item.email,
            $filter("date")(item.birthDate, "dd/MM/yyyy"),
            item.basicSalary.toLocaleString("id-ID", { style: "currency", currency: "IDR" }).replace(/[.,]/g, function (match) {
                return match === "." ? "," : ".";
            }),
            item.status,
            item.group,
            $filter("date")(item.description, "dd/MM/yyyy"),
            `<td>
                <a href="/app/#/detail/` +
                item.username +
                ` " class="badge rounded-pill text-bg-primary text-decoration-none">Detail</a>
                <button class="badge rounded-pill text-bg-warning">Edit</button>
                <button class="badge rounded-pill text-bg-danger">Delete</button>
            </td>`,
        ]);

        //update data tablenya
        $scope.table.clear();
        $scope.table.rows.add(dataTableData);
        $scope.table.draw();
        // table.ajax.reload();
    };

    $scope.resetAdvSearch = function () {
        $scope.searchOptions1 = null;
        $scope.searchParam1 = null;
        $scope.searchOptions2 = null;
        $scope.searchParam2 = null;

        $rootScope.historySearch = {};
    };

    $scope.SearchParameterOptions = [
        { label: "Username", value: "username" },
        { label: "First Name", value: "firstName" },
        { label: "last Name", value: "lastName" },
        { label: "Email", value: "email" },
        { label: "BirthDate", value: "birthDate" },
        { label: "Basic Salary", value: "basicSalary" },
        { label: "Status", value: "status" },
        { label: "Group", value: "group" },
        { label: "Description", value: "description" },
    ];

    //generate 100 dummy data
    $scope.generateEmployees = function () {
        var employees = [];
        for (var i = 0; i < 100; i++) {
            var employee = {
                username: faker.internet.userName(),
                firstName: faker.name.firstName(),
                lastName: faker.name.lastName(),
                email: faker.internet.email(),
                birthDate: faker.date.past(20, "2000-01-01"),
                basicSalary: parseInt(faker.random.number({ min: 5, max: 20 }) + "000000"), //salary 5-20 juta
                status: faker.random.arrayElement(["Active", "Nonaktif"]),
                group: faker.random.arrayElement(["Engineering", "Marketing", "Sales", "HR"]),
                description: new Date(),
            };
            employees.push(employee);
        }
        return employees;
    };

    $scope.advanceSearch = function (option1, option2, param1, param2) {
        var resultArray1 = [];
        var resultArray2 = [];

        var dataUtuh = angular.copy($scope.staticEmployees);
        param1 = $scope.checkIfDate(param1);
        param2 = $scope.checkIfDate(param2);

        for (var i in dataUtuh) {
            // cari parameter pertama
            let current1 = dataUtuh[i][option1];
            let current2 = dataUtuh[i][option2];

            if (current1 instanceof Date) {
                current1.setHours(0, 0, 0, 0);
                current1 = current1.toISOString().toLowerCase();
            } else if (typeof current1 === "string") {
                current1 = current1.toLowerCase();
            } else {
                current1 = current1.toString().toLowerCase();
            }

            if (current1.includes(param1)) {
                resultArray1.push(dataUtuh[i]);
            }

            //cari parameter kedua
            if (current2 instanceof Date) {
                current2.setHours(0, 0, 0, 0);
                current2 = current2.toISOString().toLowerCase();
            } else if (typeof current2 === "string") {
                current2 = current2.toLowerCase();
            } else {
                current2 = current2.toString().toLowerCase();
            }

            if (current2.includes(param2)) {
                resultArray2.push(dataUtuh[i]);
            }
        }

        //compare lalu ambil yang sama
        $scope.finalSearchArray = resultArray1.filter((value) => resultArray2.includes(value));
        $scope.employees = angular.copy($scope.finalSearchArray);
        console.log("Final Search Array", $scope.finalSearchArray);

        //requirement history search exist setelah lihat detail
        $rootScope.historySearch.searchOptions1 = option1;
        $rootScope.historySearch.searchOptions2 = option2;
        $rootScope.historySearch.searchParam1 = param1;
        $rootScope.historySearch.searchParam2 = param2;
        $rootScope.historySearch.searched = true;

        var dataTableData = $scope.finalSearchArray.map((item) => [
            item.username,
            item.firstName,
            item.lastName,
            item.email,
            $filter("date")(item.birthDate, "dd/MM/yyyy"),
            item.basicSalary.toLocaleString("id-ID", { style: "currency", currency: "IDR" }).replace(/[.,]/g, function (match) {
                return match === "." ? "," : ".";
            }),
            item.status,
            item.group,
            $filter("date")(item.description, "dd/MM/yyyy"),
            `<td>
                <a href="/app/#/detail/` +
                item.username +
                ` " class="badge rounded-pill text-bg-primary text-decoration-none">Detail</a>
                <button class="badge rounded-pill text-bg-warning">Edit</button>
                <button class="badge rounded-pill text-bg-danger">Delete</button>
            </td>`,
        ]);

        //update data tablenya
        $scope.table.clear();
        $scope.table.rows.add(dataTableData);
        $scope.table.draw();
        // table.ajax.reload();
    };


    //load data
    //enable salah satu diantara $scope.employees ini
    // $scope.employees = $scope.generateEmployees(); //detail issue, tidak jadi dipakai
    $scope.employees = $scope.staticEmployees; //data statis

    $scope.defaultEmployees = angular.copy($scope.employees); //keperluan search by parameter

    console.log("employees", $scope.defaultEmployees);
    console.log("parameter", $scope.SearchParameterOptions);

    // Load Data Table setelah view di render
    $timeout(function () {
        $scope.table = $("#employeeTable").DataTable({
            pageLength: 25,
            scrollCollapse: true,
            columnDefs: [
                {
                    targets: [9],
                    orderable: false,
                },
                {
                    targets: [5],
                    className: "text-end",
                    type: "currency",
                },
            ],
            searching: false,
            search: {
                search: null,
            },
        });

        if ($rootScope.historySearch.searched === true) {
            $scope.searchBar = true;
            $scope.searchParam1 = $rootScope.historySearch.searchParam1;
            $scope.searchParam2 = $rootScope.historySearch.searchParam2;
            $scope.searchOptions1 = $rootScope.historySearch.searchOptions1;
            $scope.searchOptions2 = $rootScope.historySearch.searchOptions2;
            $scope.advanceSearch($scope.searchOptions1, $scope.searchOptions2, $scope.searchParam1, $scope.searchParam2);
        }
    }, 0);
});
