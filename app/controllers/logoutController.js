app.controller('LogoutController', ['$scope', '$location', '$window', 'AuthService', function($scope, $location, $window, AuthService) {
    AuthService.logout();
    $location.path('/login');
    
    $scope.$applyAsync(function() {
        $window.location.reload();
    });
}]);
