# Employee-Angular



## Getting started

### English
1. Clone this repository using ``` git clone https://gitlab.com/rizki.dwikurnia14/employee-angular.git ``` in your terminal
2. Open the folder and install dependencies using ```npm install``` in the terminal and wait until it's done
3. Run the app by typing ```http-server -c-1``` in the terminal
4. Access the app using the link provided in the terminal, it should look like this: http://127.0.0.1:8080/app/
5. log in by inserting both "user" in the username and password
6. it'done! you can use the app by now.

### Indonesian
1. Clone repositori ini dengan mengetikan  ``` git clone https://gitlab.com/rizki.dwikurnia14/employee-angular.git ``` di terminal
2. Buka folder dan install dependency dengan mengetik ```npm install``` di terminal dan tunggu sampai selesai
3. Jalankan aplikasi dengan mengetik ```http-server -c-1``` di terminal
4. Buka aplikasinya menggunakan link yang tersedia, link seharusnya terlihat seperti ini: http://127.0.0.1:8080/app/
5. Masuk dengan mengetikan "user" di field username dan password
6. Selesai! aplikasi siap digunakan
